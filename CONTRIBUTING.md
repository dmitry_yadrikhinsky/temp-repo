# Guidelines

## For each pull request

After you've created PR, you should bump version in `package.json` (e.g 0.6.3 → 0.6.4-0).
Also please add a short description of your changes in `changelog.md`.
To find available version you can execute `npm run get-versions`.

## Versioning

1. increment patch version for backwards-compatible bugfix (e.g. 0.6.3 → 0.6.4)
1. increment minor version for backwards-compatible new functionality (e.g. 0.6.3 → 0.7.0)
1. increment major version for breaking changes (e.g. 0.6.3 → 1.0.0)
More about [Semantic Versioning](https://semver.org/)

## If you know how to make this guideline better

Please update `CONTRIBUTING.md` file.